#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -la'
alias shrug='echo "¯\_(ツ)_/¯" | xclip -selection c'

# Git branchname on commandline
GIT_PROMPT=/usr/share/git/completion/git-prompt.sh
GIT_COMPLETION=/usr/share/git/completion/git-completion.bash
[[ -f $GIT_PROMPT ]] && source $GIT_PROMPT 
[[ -f $GIT_COMPLETION ]] && source $GIT_COMPLETION
GIT_PS1_SHOWDIRTYSTATE=true
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \[\033[01;31m\]$(__git_ps1 "(%s) 
")\[\033[01;34m\]\$\[\033[00m\] '

export STEAM_FRAME_FORCE_CLOSE=1

# Export user bin
[[ -d $HOME/bin ]] && export PATH=$HOME/bin:$PATH
