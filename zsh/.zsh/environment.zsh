#                 ██      
#                ░██      
#  ██████  ██████░██      
# ░░░░██  ██░░░░ ░██████  
#    ██  ░░█████ ░██░░░██ 
#   ██    ░░░░░██░██  ░██ 
#  ██████ ██████ ░██  ░██ 
# ░░░░░░ ░░░░░░  ░░   ░░  
#
#  ▓▓▓▓▓▓▓▓▓▓
# ░▓ author ▓ xero <x@xero.nu>
# ░▓ code   ▓ http://code.xero.nu/dotfiles
# ░▓ mirror ▓ http://git.io/.files
# ░▓▓▓▓▓▓▓▓▓▓
# ░░░░░░░░░░
#
#█▓▒░ timestamps
#HIST_STAMPS=mm/dd/yyyy

#█▓▒░ paths
export PATH=$HOME/bin:/usr/local/bin:$PATH
#export MANPATH=/usr/local/man:$MANPATH

#█▓▒░ preferred editor for local and remote sessions
export EDITOR=vim
export VISUAL=vim

#█▓▒░ language
export LC_COLLATE=en_GB.UTF-8
export LC_CTYPE=en_GB.UTF-8
export LC_MESSAGES=en_GB.UTF-8
export LC_MONETARY=nl_NL.UTF-8
export LC_NUMERIC=nl_NL.UTF-8
export LC_TIME=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8
export LANG=en_GB.UTF-8
export LANGUAGE=en_GB.UTF-8
export LESSCHARSET=utf-8

#█▓▒░ custom
export STEAM_FRAME_FORCE_CLOSE=1
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"


export XDG_CONFIG_HOME=/home/berry
